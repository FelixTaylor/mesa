```
                      ___  ___ _____ _____  ___
                      |  \/  ||  ___/  ___|/ _ \
                      | .  . || |__ \ `--./ /_\ \
                      | |\/| ||  __| `--. \  _  |
                      | |  | || |___/\__/ / | | |
                      \_|  |_/\____/\____/\_| |_/

```

MESA is a text based rogue-like dungeon creeper - well not yet, maybe some day tho. You are fighting your way up through
hords of creatures and beasts to reach the desired top of the mesa.

## How to play

For this being a text based game there is no kind of graphical user-interface and also mouse support is missing. To
perform an action write the desired command. You can choose between a number of actions like `FIGHT` or `GO TO` or
use `NORTH`, `EAST`, `SOUTH` and `WEST` for direct movement through rooms. In these `Room`s you will sometimes have to
kill all living `Creature` or find specific items like keys.

## Level concept

A level in `MESA` contains a number of `Room`s which the player can explore. Each `Room` has `Tile`s where the `Player`
can move on.

```
   +------------+       +-------+      +------------------------+
   | GameObject | <---- | Level | ---- | HashMap<Integer, Room> |
   +------------+       +-------+      +------------------------+
```

### Rooms

One or more `Room`s are part of a single `Level`. These `Room`s are connected by `Door`s. In each `Room` there is at
least one `Door` and four doors at most. Every door is leading the `Player` either to the *north, west, east* or the
*south*.

### Tile

A `Tile` is the smallest unit in `MESA` it size can **not** be altered. On each
`Tile` a number of `Creature`s and objects are spawned. One `Tile` does not know its surrounding `Tile`s and a fight
takes place only on one `Tile` at the time.

```
        <- Room  ->
       +-----------+
       |   |   |   |
       +---+---+---+
       |   |   |   |
       +---+---+---+    +----------------------+
       |   | x-------------->   Tile.class     |
       +-----------+    |                      |
                        | name        : String |
                        | description : String |
                        | exit        : Exit   |
                        | creatures   : List   |
                        |                      |
                        +----------------------+
                         <---  Tile [1,2]  --->
```

The default room has a size of 3x3 like the example below. Other rooms may be bigger or smaller. Each room can have at
least one exit which should be placed in the center of the outer walls and not in one of the corners. The exits are
marked as `x`.

```
Single room:

               N                         N
         +-----------+             +-----------+
         |   | x |   |             |0,0|1,0|2,0|            N: North
         |---+---+---|             |---+---+---|            E: East
       w | x |   | x | E         W |0,1|1,1|2,1| E          S: South
         |---+---+---|             |---+---+---|            W: West
         |   | x |   |             |0,2|1,2|2,2|            X: Room exit
         +-----------+             +-----------+
               S                         S
```

Here is an example of the training level containing three `Room`s with two doors. Two of the rooms have the default
size, the other one is a bit larger. Each `x`
marks a `Tile` where the `Player` can move to the next adjacent room. Note that each room has its own coordinate system,
meaning each room has a [0,0]
coordinate.

```
          
         
          <- Room1 -> <----- Room2 ----->
         +---+---+---+---+---+---+---+---+
         |0,0|1,0|2,0|0,0|1,0|2,0|3,0|4,0|
         +---+---+---+---+---+---+---+---+
         |0,1|1,1| x | x |1,1|2,1|3,1|4,1|
         +---+---+---+---+---+---+---+---+
         |0,2|1,2|2,2|0,2|1,2| x |3,2|4,2|
         +---+---+---+---+---+---+---+---+
                         |0,0| x |2,0|
                         +---+---+---+
                         |0,1|1,1|2,1|
                         +---+---+---+
                         |0,2|1,2|2,2|
                         +-----------+
                          <- Room3 -> 

```

