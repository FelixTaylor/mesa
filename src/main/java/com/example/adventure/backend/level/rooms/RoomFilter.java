package com.example.adventure.backend.level.rooms;


import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.actions.teleport.Findable;
import com.example.adventure.backend.characters.creatures.Creature;
import com.example.adventure.backend.level.components.Tile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static java.util.Objects.isNull;

@Component
public class RoomFilter {

    @Autowired
    final Room room;

    public RoomFilter(final Room room) {
        this.room = room;
    }

    public boolean canGoThroughNextDoor() {
        final Tile[][] tiles = room.getTiles();
        boolean canGoThroughNextDoor = true;
        for (Tile[] tile : tiles) {
            for (Tile t : tile) {
                if (!t.allCreaturesArDead()) {
                    canGoThroughNextDoor = false;
                    break;
                }
            }
        }
        return canGoThroughNextDoor;
    }

    public Position getPositionOfDoorByTargetId(final int id) {
        for (int x=0; x<room.getTiles().length; x++) {
            for (int y=0; y<room.getTiles()[0].length; y++) {
                final Tile tile = room.getTiles()[x][y];
                if (tile.hasDoor() && tile.getDoor().getTargetId() == id) {
                    return new Position(x, y);
                }
            }
        }
        return null;
    }

    public Position getPositionOfObject(final Findable findable) {
        final Tile[][] tiles = room.getTiles();
        final int maxX = tiles.length;
        final int maxY = tiles[0].length;
        for (int x = 0; x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                if (findableIsOnTile(findable, tiles[x][y])) {
                    return new Position(x, y);
                }
            }
        }
        return null;
    }

    private boolean findableIsOnTile(final Findable findable, final Tile tile) {
        if (isNull(tile) || isNull(tile.getDoor())) {
            return false;
        }
        final String doorName = tile.getDoor().getName();
        return tile.hasDoor() && findable.getName().equals(doorName);
    }

    public boolean atLeastOneCreatureIsAlive(final Position playerPosition) {
        final Tile tile = room.getTiles()[playerPosition.getX()][playerPosition.getY()];
        return tile.getCreatures().stream()
                .filter(Objects::nonNull)
                .anyMatch(Creature::isAlive);
    }
}
