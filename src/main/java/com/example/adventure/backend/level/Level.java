package com.example.adventure.backend.level;

import com.example.adventure.backend.level.rooms.Room;
import com.example.adventure.backend.misc.GameObject;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class Level extends GameObject {
    private final HashMap<Integer, Room> rooms;

    public Level() {
        this.rooms = new HashMap<>();
        setName("Level");
    }

    public Level(final HashMap<Integer, Room> rooms) {
        this();
        this.rooms.putAll(rooms);
    }

    public Level withRoom(final Room room) {
        final int position = this.rooms.size() +1;
        this.rooms.put(position, room);
        return this;
    }

    public Level withName(final String name) {
        setName(name);
        return this;
    }

    public Level withDescription(final String description) {
        setDescription(description);
        return this;
    }

    public Room getRoom(final int id) {
        return rooms.get(id);
    }
}
