package com.example.adventure.backend.level.components;

import com.example.adventure.backend.characters.creatures.Creature;
import com.example.adventure.backend.level.rooms.Door;
import com.example.adventure.backend.misc.GameObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

import static com.example.adventure.backend.misc.Messages.*;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Comparator.comparing;
import static java.util.Objects.nonNull;

@ToString
@Component
public class Tile extends GameObject {

    @Getter
    private ArrayList<Creature> creatures;

    @Getter
    @Setter
    private Door door;

    public Tile() {
        creatures = new ArrayList<>();
    }

    public boolean hasDoor() {
        return nonNull(getDoor());
    }

    public void setCreatures(final Creature... creatures) {
        this.creatures = new ArrayList<>();
        this.creatures.addAll(asList(creatures));
    }

    public String creaturesAsString() {
        final HashMap<String, Creature> distinctCreatures = new HashMap<>();
        getCreatures().forEach(c -> distinctCreatures.put(c.getName(), c));

        int counter = 0;
        final StringBuilder out = new StringBuilder();
        for (final Map.Entry<String, Creature> entry : distinctCreatures.entrySet()) {
            final Creature creature = entry.getValue();
            final long times = getCreatures().stream().filter(creatureWithSameName(creature)).count();
            final boolean isSingleCreature = times == 1;
            final String name = creature.getName();

            if (creature.getHitPoints() == 0) {
                if (counter == 0)
                    out.append(isSingleCreature
                            ? format(CREATURE_DEAD_IS_PRESENT, name)
                            : format(CREATURES_DEAD_ARE_PRESENT, times, name));
                else
                    out.append(isSingleCreature
                            ? format(CREATURE_DEAD_IS_PRESENT_AND, name)
                            : format(CREATURES_DEAD_ARE_PRESENT_AND, times, name));
            } else {
                if (counter == 0)
                    out.append(isSingleCreature
                            ? format(CREATURE_IS_PRESENT, name)
                            : format(CREATURES_ARE_PRESENT, times, name));
                else
                    out.append(isSingleCreature
                            ? format(CREATURE_IS_PRESENT_AND, name)
                            : format(CREATURES_ARE_PRESENT_AND, times, name));
            }
            counter++;
            out.append(" ");
        }
        return out.toString().trim();
    }

    private Predicate<Creature> creatureWithSameName(Creature creature) {
        return c -> c.getName().equals(creature.getName());
    }

    public void sortCreaturesList() {
        creatures.sort(comparing(Creature::getName));
    }

    public boolean allCreaturesArDead() {
        return getCreatures().stream()
                .filter(Objects::nonNull)
                .noneMatch(Creature::isAlive);
    }

    @Override
    public String getDescription() {
        // todo rework: Remove hint that no creature is present
        if (super.getDescription().equals("<No Description>")) {
            String message = hasDoor() ? "You see a door. " : "";
            if (!getCreatures().isEmpty())
                message += "There " + creaturesAsString() + " here.";
            return message;
        }
        return super.getDescription();
    }

}
