package com.example.adventure.backend.level.rooms;

import com.example.adventure.backend.level.components.Tile;
import com.example.adventure.backend.misc.GameObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

@ToString
@Component
public class Room extends GameObject {
    private boolean completionMessageWasShown;

    @Setter
    @Getter
    private Tile[][] tiles;

    public Room() {
        this.tiles = new Tile[3][3];
        this.completionMessageWasShown = false;
    }

    public boolean completionMessageWasShown() {
        return completionMessageWasShown;
    }

    public void completionMessageWasShown(final boolean completionMessageWasShown) {
        this.completionMessageWasShown = completionMessageWasShown;
    }
}
