package com.example.adventure.backend.level.rooms;


import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.characters.creatures.Creature;
import com.example.adventure.backend.level.components.Tile;
import com.example.adventure.backend.level.components.TileBuilder;

import java.util.Random;

import static java.lang.String.format;

public class RoomBuilder {
    private final Room room;

    public RoomBuilder() {
        this.room = new Room();
    }

    public RoomBuilder withCreatures(final Position position, final Creature... creatures) {
        final int x = position.getX();
        final int y = position.getY();
        room.getTiles()[x][y].setCreatures(creatures);
        return this;
    }

    public RoomBuilder withCreatures(final Creature... creatures) {
        final Random rand = new Random();
        for (final Creature c : creatures) {
            final int x = rand.nextInt(room.getTiles().length);
            final int y = rand.nextInt(room.getTiles()[0].length);
            room.getTiles()[x][y].setCreatures(c);
        }
        return this;
    }

    public RoomBuilder withName(final String name) {
        room.setName(name);
        return this;
    }

    public RoomBuilder withDoor(final Position position, final Door door) {
        final int x = position.getX();
        final int y = position.getY();
        room.getTiles()[x][y].setDoor(door);
        return this;
    }

    public RoomBuilder withDescription(final String description) {
        room.setDescription(description);
        return this;
    }

    public RoomBuilder withTiles(final int x, final int y) {
        final Tile[][] tiles = new Tile[x][y];
        for(int i=0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[0].length; j++) {
                final String name = room.getName() + "[%d][%d]:";
                tiles[i][j] = new TileBuilder()
                        .withName(format(name, i, j))
                        .build();
            }
        }
        room.setTiles(tiles);
        return this;
    }

    public RoomBuilder withDefaultTiles() {
        withTiles(3,3);
        return this;
    }

    public Room build() {
        return room;
    }

}
