package com.example.adventure.backend.level.rooms;


import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.misc.GameObject;

public class DoorBuilder extends GameObject {
    private final Door door;

    public DoorBuilder() {
        this.door = new Door();
    }

    public DoorBuilder withName(final String name) {
        this.door.setName(name);
        return this;
    }

    public DoorBuilder withTargetId(final int id) {
        this.door.setTargetId(id);
        return this;
    }

    public DoorBuilder withTargetPosition(final Position pos) {
        this.door.setTargetPosition(pos);
        return this;
    }

    public Door build() {
        if (door.getTargetId() == -1)
            throw new IllegalStateException("Target id must be set.");
        return this.door;
    }
}
