package com.example.adventure.backend.level;

import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.level.components.LevelBuilder;
import com.example.adventure.backend.level.rooms.Door;
import com.example.adventure.backend.level.rooms.DoorBuilder;
import com.example.adventure.backend.level.rooms.Room;
import com.example.adventure.backend.level.rooms.RoomBuilder;

import static com.example.adventure.backend.actions.teleport.Findable.*;
import static com.example.adventure.backend.characters.creatures.CreatureFactory.*;

public class TrainingLevel implements LevelBuilder {
    public Level get() {
        final String description = "This seems like a large dungeon with many rooms to cross in order to reach the top and fight the giant. In the east there is a door.";

        return new Level().withName("Training Level")
                .withDescription(description)
                .withRoom(livingRoom())
                .withRoom(bedroom())
                .withRoom(finalRoom());
    }

    private Room livingRoom() {
        final String name = "Living room";
        final String description = "You are standing in an untidy room, old dishes are laying around and a few rats are crawling over the floor.";

        final Door doorEast = new DoorBuilder()
                .withName(EAST_DOOR.getName())
                .withTargetPosition(new Position(0, 1))
                .withTargetId(2)
                .build();

        return new RoomBuilder()
                .withName(name)
                .withDefaultTiles()
                .withDoor(new Position(2, 1), doorEast)
                .withCreatures(new Position(1,1), getMeatBug(), getRat(), getRat())
                .withCreatures(new Position(1,0), getRat())
                .withCreatures(new Position(2,2), getRat())
                .withDescription(description)
                .build();
    }

    private Room bedroom() {
        final String name = "Bedroom";
        final String description = "This bedroom has a bed with a dog on it, which looks hungry.";
        final Door westDoor = new DoorBuilder()
                .withName(WEST_DOOR.getName())
                .withTargetPosition(new Position(2,1))
                .withTargetId(1)
                .build();

        final Door southDoor = new DoorBuilder()
                .withName(SOUTH_DOOR.getName())
                .withTargetPosition(new Position(0, 1))
                .withTargetId(3)
                .build();

        return new RoomBuilder()
                .withName(name)
                .withTiles(4,3)
                .withDoor(new Position(0, 1), westDoor)
                .withDoor(new Position(2, 2), southDoor)
                .withCreatures(getRat(), getMeatBug(), getFeralDog())
                .withDescription(description)
                .build();
    }

    private Room finalRoom() {
        final String description = "Boss fight room";
        final String name = "Final Room";

        final Door northDoor = new DoorBuilder()
                .withName(NORTH_DOOR.getName())
                .withTargetPosition(new Position(2,2))
                .withTargetId(2)
                .build();

        return new RoomBuilder()
                .withName(name)
                .withDefaultTiles()
                .withDoor(new Position(0,1), northDoor)
                .withDescription(description)
                .build();
    }
}
