package com.example.adventure.backend.level.components;

import com.example.adventure.backend.characters.creatures.Creature;
import com.example.adventure.backend.level.rooms.Door;

public class TileBuilder {
    private final Tile tile;

    public TileBuilder() {
        this.tile = new Tile();
    }

    public TileBuilder withName(final String name) {
        tile.setName(name);
        return this;
    }

    public TileBuilder withCreatures(final Creature... creatures) {
        tile.setCreatures(creatures);
        tile.sortCreaturesList();
        return this;
    }

    public TileBuilder withExit(final Door door) {
        tile.setDoor(door);
        return this;
    }

    public TileBuilder withDescription(final String description) {
        tile.setDescription(description);
        return this;
    }

    public Tile build() {
        return tile;
    }
}
