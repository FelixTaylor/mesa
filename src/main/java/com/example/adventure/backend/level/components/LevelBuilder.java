package com.example.adventure.backend.level.components;

import com.example.adventure.backend.level.Level;

public interface LevelBuilder {
    Level get();
}
