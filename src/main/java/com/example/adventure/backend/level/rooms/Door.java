package com.example.adventure.backend.level.rooms;

import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.misc.GameObject;
import lombok.Data;

@Data
public class Door extends GameObject {
    private Position targetPosition;
    private boolean isOpen;
    private int targetId;

    public Door() {
        this.targetPosition = new Position(0,1);
        this.isOpen = false;
        this.targetId = -1;
        setName("Door");
    }
}
