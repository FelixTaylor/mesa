package com.example.adventure.backend.main;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component
public class Config {

    @Getter
    private final Properties properties;

    public Config() {
        this.properties = new Properties();
    }

    public void load() {
        final ClassLoader loader = getClass().getClassLoader();
        try (final InputStream input = loader.getResourceAsStream("application.properties")) {
            properties.load(input);
        } catch (IOException ex) {
            throw new RuntimeException("Could not load application.properties");
        }
    }

    public boolean isDeveloper() {
        return get("game.user.isDeveloper").equals("true");
    }

    public String get(final String config) {
        return getProperties().getProperty(config);
    }
}
