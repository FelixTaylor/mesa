package com.example.adventure.backend.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.nio.file.Path;

import static java.nio.file.Paths.get;

public class Persistence {
    public static final String LOAD = "load";
    public static final String SAVE = "save";
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static final String os = System.getProperty("os.name");
//    private static final String jkd = System.getProperty("java.specification.version");
    private final Path path;

    public Persistence() {
        path = os.equals("Linux")
                ? get("savegame", " mesa.save")//get("./saves/mesa.save")
                : get("");
        // todo implement save folder for windows
        // todo check jdk version
    }

//    public void save(final Data data) {
//        // todo saved game is removed in docker container after shutting down
//        final byte[] content = gson.toJson(data).getBytes();
//        try {
//            if (exists(path)) delete(path);
//            write(path, content, CREATE);
//            if (exists(path)) printS(SYS_CHARACTER_SAVED);
//        } catch (IOException e) {
//            e.printStackTrace();
//            printS(SYS_COULD_NOT_SAVE);
//        }
//    }
//
//    public Data load() {
//        try {
//            final Data data = gson.fromJson(readString(path), Data.class);
//            if (nonNull(data)) printS(SYS_CHARACTER_LOADED);
//            return data;
//        } catch (IOException e) {
//            e.printStackTrace();
//            printS(SYS_COULD_NOT_LOADED);
//        }
//        return new Data();
//    }
}
