package com.example.adventure.backend.misc;

import java.util.regex.Matcher;

import static com.example.adventure.backend.misc.Messages.*;
import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.regex.Pattern.compile;

public class InputFilter {
    public static final String CMD = "command";
    public static final String OBJ = "object";

    /**
     * Match the first word of the string which is followed by a whitespace.
     */
    private final String commandPattern = "(^\\w+)\\s+";

    /**
     * Match every word which is followed by one or more whitespaces or words.
     * Following whitespaces and words can occur multiple times.
     */
    private final String objectPattern = "\\s+([\\w+|\\s+]*$)";

    public InputFilter() { }

    public boolean isNullOrEmpty(final String input) {
        return isNull(input) || input.trim().isEmpty() || input.trim().isBlank();
    }

    public String filter(final String filter, final String input) {
        if (isNullOrEmpty(input)) {
            throw new IllegalArgumentException(format(SYS_MAY_NOT_BE_NULL, "Input"));
        } else if (isNullOrEmpty(filter)) {
            throw new IllegalArgumentException(format(SYS_MAY_NOT_BE_NULL, "Filter"));
        }

        return switch (filter) {
            case "command" -> getResult(commandPattern, input);
            case "object"  -> getResult(objectPattern, input);
            default -> throw new IllegalStateException("Unexpected value: " + filter);
        };
    }

    public boolean isInputValid(final String input) {
        return input.split("\\s+").length >= 2;
    }

    private String getResult(final String pattern, final String input) {
        final Matcher matcher = getMatcherWith(pattern, input);
        return !checkIfPatternWasMatched(matcher)
                ? SYS_NOT_A_VALID_COMMAND
                : matcher.group(1);
    }

    private boolean checkIfPatternWasMatched(final Matcher matcher) {
        if (!matcher.find()) {
            formatS(SYS_COULD_NOT_FIND_COMMAND);
            return false;
        }
        return true;
    }

    private Matcher getMatcherWith(final String pattern, final String input) {
        return compile(pattern).matcher(trimToLowerCase(input));
    }
}
