package com.example.adventure.backend.misc;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class GameObject {

    private String name;
    private String description;

    public GameObject() {
        this.name = "<GameObject>";
        this.description = "<No Description>";
    }
}
