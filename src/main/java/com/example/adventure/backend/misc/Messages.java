package com.example.adventure.backend.misc;

import static java.lang.String.format;

public class Messages {
    public static String getPositionFromInt(final int num) {
        String out = Integer.toString(num);
        switch (num) {
            case 1 -> out += "st";
            case 2 -> out += "nd";
            case 3 -> out += "rd";
            default -> out += "th";
        }
        return out;
    }

    public static void promptUserInput() {
        print("> ");
    }

    public static String trimToLowerCase(final String in) {
        return in.trim().toLowerCase();
    }

    private static void print(final String out) {
        System.out.print(out);
    }

    private static void println(final String out) {
        print(out + "\n");
    }

    private static String formatln(final String input, final Object... objs) {
        return format(input, objs);
    }

    public static String printS(final String message) {
        return formatln(PREFIX_SYSTEM, message);
    }

    public static String formatN(final String input, final Object... strs) {
        final String message = format(input, strs).trim();
        if (!message.isBlank())
            return formatln(PREFIX_NORMAL, message);
        return EMPTY;
    }

    public static String formatS(final String input, final Object... strs) {
        final String message = format(input, strs).trim();
        if (!message.isBlank())
            return formatln(PREFIX_SYSTEM, message);
        return EMPTY;
    }

    public static String formatF(final String input, final Object... strs) {
        final String message = format(input, strs).trim();
        if (!message.isBlank())
            return formatln(PREFIX_FIGHT, message);
        return EMPTY;
    }

    public static final String EMPTY = "";
    public static final String PREFIX_NORMAL = "%s";
    public static final String PREFIX_SYSTEM = "%s";
    public static final String PREFIX_FIGHT = "%s";
    public static final String SYS_CREATURE_LIST_ELEMENT = "%s %d %s";
    public static final String SYS_COULD_NOT_FIND_COMMAND = "Could not find command.";
    public static final String SYS_NOT_A_VALID_COMMAND = "Not a valid command";
    public static final String SYS_MAY_NOT_BE_NULL = "%s may NOT be null or empty.";
    public static final String SYS_CHARACTER_LOADED = "Loaded game.";
    public static final String SYS_COULD_NOT_LOADED = "Could not load game.";
    public static final String SYS_COULD_NOT_SAVE = "Could not save game.";
    public static final String SYS_CHARACTER_SAVED = "Game progression saved.";
    public static final String SYS_START_POS_NULL = "start position is null";
    public static final String SYS_INPUT_IS_NO_VALID_COMMAND = "Sorry, but '%s' is not a command.";

    // Interaction with environment
    public static final String ENV_NO_DOOR_PRESENT = "There is no door here.";
    public static final String ENV_CAN_NOT_USE_DOOR = "The door is closed. Maybe there's something in here.";
    public static final String ENV_ASK_WHERE_TO = "Where do you want to go? %s";
    public static final String ENV_THE_ROOM_ENDS_HERE = "You can't go further, the room ends here.";

    public static final String YOU_COULD_NOT_GO_TO = "There is no %s.";
    public static final String YOU_MOVED_TO = "You moved to the %s.";

    // Interaction with creatures
    public static final String CREATURE_IS_PRESENT = "is a %s";
    public static final String CREATURE_IS_PRESENT_AND = "and a %s";
    public static final String CREATURE_DEAD_IS_PRESENT = "is a dead %s";
    public static final String CREATURE_DEAD_IS_PRESENT_AND = "and a dead %s";
    public static final String CREATURES_ARE_PRESENT = "are %d %ss";
    public static final String CREATURES_ARE_PRESENT_AND = "and %d %ss";
    public static final String CREATURES_DEAD_ARE_PRESENT = "are %d dead %ss";
    public static final String CREATURES_DEAD_ARE_PRESENT_AND = "and %d dead %ss";
    public static final String CREATURE_DEALS_NO_DAMAGE = "The %s misses it's hit.";
    public static final String CREATURE_DEALS_DAMAGE = "%s %s dealing %s damage.";
    public static final String CREATURE_DEALS_CRIT_DAMAGE = "The %s %s dealing %s critical damage.";
    public static final String CREATURES_ARE_ALREADY_DEAD = "The creature is already dead.";
    public static final String NO_CREATURE_OF_THAT_TYPE_PRESENT = "There is no %s present here.";
    public static final String NO_CREATURES_TO_ATTACK = "There are no creatures to attack.";
    public static final String ALL_CREATURES_DEAD_IN_ROOM = "All creatures are dead, you can move on through the next door.";

    public static final String YOU_DEAL_DAMAGE = "You are %sing dealing %s damage.";
    public static final String YOU_KILLED_THE = "You killed the %s.";
    public static final String YOU_DEAL_CRIT_DAMAGE = "You are performing a critical %s dealing %s damage.";
    public static final String YOU_DEAL_NO_DAMAGE = "You miss your next hit.";

    public static final String PLAYER_LIFE = "You have %s hit points.";
    public static final String YOU_DIED = "You Died!";

    public static final String WHERE_TO_GO = "Where do you want to go to?";

    public static final String MESA_INTRO = """
            Welcome to the
            ___  ___ _____ _____  ___
            |  \\/  ||  ___/  ___|/ _ \\
            | .  . || |__ \\ `--./ /_\\ \\
            | |\\/| ||  __| `--. \\  _  |
            | |  | || |___/\\__/ / | | |
            \\_|  |_/\\____/\\____/\\_| |_/
            """;

}
