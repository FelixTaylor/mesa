package com.example.adventure.backend;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Data
@Component
public class Text {

    @Getter
    @Setter
    private String positionText;

    @Getter
    @Setter
    private String tileDescription;

    @Getter
    @Setter
    private String action;

    @Getter
    @Setter
    private String options;

    @Getter
    @Setter
    private String error;

    public Text() {
        this.tileDescription = "";
        this.positionText = "[0,0]";
        this.options = "";
        this.action = "";
        this.error = "";
    }
}
