package com.example.adventure.backend.actions.move;

import com.example.adventure.backend.Adventure;
import com.example.adventure.backend.characters.player.Player;
import com.example.adventure.backend.level.components.Tile;
import com.example.adventure.backend.level.rooms.Door;
import com.example.adventure.backend.level.rooms.Room;
import com.example.adventure.backend.level.rooms.RoomFilter;

import java.util.ArrayList;

import static com.example.adventure.backend.misc.Messages.*;
import static java.util.Objects.isNull;

public class Move {
    public static final String NORTH = "north";
    public static final String EAST = "east";
    public static final String SOUTH = "south";
    public static final String WEST = "west";
    public static final String ENTER = "enter";
    public static final String GO = "go";

    private final Adventure adventure;

    public Move(final Adventure adventure) {
        this.adventure = adventure;
    }

    private int getNextRoomId(final Position position) {
        final int x = position.getX();
        final int y = position.getY();
        final Tile tile = adventure.getCurrentRoom().getTiles()[x][y];
        return tile.hasDoor()
                ? tile.getDoor().getTargetId()
                : 0;
    }

    public String getPossibleDirectionsToMove(final Player player) {
        final Tile[][] tiles = adventure.getCurrentRoom().getTiles();
        final ArrayList<String> directions = new ArrayList<>();
        final int x = player.getX();
        final int y = player.getY();

        if (y>0)                directions.add(NORTH);
        if (x>0)                directions.add(WEST);
        if (y<tiles.length)     directions.add(SOUTH);
        if (x<tiles[1].length)  directions.add(EAST);

        final StringBuilder builder = new StringBuilder();
        for (String s : directions)
            builder.append(s).append(" ");

        return builder.toString();
    }

    public String enter() {
        int x = getPlayer().getPosition().getX();
        int y = getPlayer().getPosition().getY();
        final Door door = adventure.getCurrentRoom().getTiles()[x][y].getDoor();
        if (isNull(door)) {
            return formatN(ENV_NO_DOOR_PRESENT);
        } else if (!door.isOpen()) {
            return formatN(ENV_CAN_NOT_USE_DOOR);
        }

        final int nextRoomId = getNextRoomId(getPlayer().getPosition());
        final Room nextRoom = adventure.getLevel().getRoom(nextRoomId);

        final RoomFilter filter = new RoomFilter(nextRoom);
        final Position position = filter.getPositionOfDoorByTargetId(getPlayer().getCurrentRoomId());
        final Door target = nextRoom.getTiles()[position.getX()][position.getY()].getDoor();
        target.setOpen(true);
        getPlayer().setCurrentRoomId(nextRoomId);
        getPlayer().setX(position.getX());
        getPlayer().setY(position.getY());
        return formatN(nextRoom.getDescription());
    }

    private Player getPlayer() {
        return this.adventure.getPlayer();
    }

    public String up() {
        final int newY = adventure.getPlayer().getY() - 1;
        if (newY >= 0) {
            adventure.getPlayer().setY(newY);
            return EMPTY;
        }
        return formatN(ENV_THE_ROOM_ENDS_HERE);
    }

    public String down() {
        final int newY = adventure.getPlayer().getY() + 1;
        if (newY < adventure.getCurrentRoom().getTiles()[0].length) {
            adventure.getPlayer().setY(newY);
            return EMPTY;
        }
        return formatN(ENV_THE_ROOM_ENDS_HERE);
    }

    public String right() {
        final int newX = adventure.getPlayer().getX() + 1;
        if (newX < adventure.getCurrentRoom().getTiles().length) {
            adventure.getPlayer().setX(newX);
            return EMPTY;
        }
        return formatN(ENV_THE_ROOM_ENDS_HERE);
    }

    public String left() {
        final int newX = adventure.getPlayer().getX() - 1;
        if (newX >= 0) {
            adventure.getPlayer().setX(newX);
            return EMPTY;
        }
        return ENV_THE_ROOM_ENDS_HERE;
    }
}
