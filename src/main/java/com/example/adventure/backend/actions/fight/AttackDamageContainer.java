package com.example.adventure.backend.actions.fight;


import com.example.adventure.backend.characters.creatures.Attack;
import lombok.Getter;

@Getter
public class AttackDamageContainer {
    private final Attack attack;
    private final double damage;

    public AttackDamageContainer(final Attack attack, final double damage) {
        this.attack = attack;
        this.damage = damage;
    }

    public String getName() {
        return getAttack().getName();
    }

}
