package com.example.adventure.backend.actions.fight;

import com.example.adventure.backend.characters.LivingEntity;
import com.example.adventure.backend.characters.PlayerBuilder;

import java.text.DecimalFormat;

import static com.example.adventure.backend.misc.Messages.*;
import static java.lang.String.format;

public class DamageApplier {
    private final DecimalFormat decimal = new DecimalFormat("#.##");
    private boolean attackerIsPlayer;
    private LivingEntity attacker;
    private LivingEntity defender;

    public DamageApplier() {
    }

    public DamageApplier withAttacker(final LivingEntity attacker) {
        if (attacker.getName().equals(PlayerBuilder.name())) {
            attackerIsPlayer = true;
        }
        this.attacker = attacker;
        return this;
    }

    public DamageApplier withDefender(final LivingEntity defender) {
        this.defender = defender;
        return this;
    }

    public String apply() {
        final Toss toss = new Toss();
        final String attackerName = attackerIsPlayer ? "You" : attacker.getName();
        final AttackDamageContainer container = attacker.getAttackDamage();
        double damage = container.getDamage();

        String message;
        if (toss.isBlunder()) {
            message = attackerIsPlayer
                    ? YOU_DEAL_NO_DAMAGE
                    : format(CREATURE_DEALS_NO_DAMAGE, attackerName);
            damage = 0;
        } else if (toss.isCritical()) {
            damage *= 2;
            message = attackerIsPlayer
                    ? format(YOU_DEAL_CRIT_DAMAGE, container.getName(), decimal.format(damage))
                    : format(CREATURE_DEALS_CRIT_DAMAGE, attackerName, container.getName(), decimal.format(damage));
        } else {
            message =  format(CREATURE_DEALS_DAMAGE, attackerName, container.getName(), decimal.format(damage));
        }
        defender.reduceHealth(damage);
        return message;
    }
}
