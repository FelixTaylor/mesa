package com.example.adventure.backend.actions.fight;

import com.example.adventure.backend.misc.Dice;
import lombok.Getter;

@Getter
public class Toss {
    private final int toss;

    public Toss() {
        toss = new Dice(1, 20).roll().sum();
    }

    public boolean isBlunder() {
        return getToss() == 1;
    }

    public boolean isCritical() {
        return getToss() >= 19;
    }
}
