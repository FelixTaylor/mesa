package com.example.adventure.backend.actions.fight;

import com.example.adventure.backend.characters.creatures.Creature;
import com.example.adventure.backend.characters.creatures.CreatureFilter;
import com.example.adventure.backend.characters.player.Player;
import com.example.adventure.backend.level.components.Tile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

import static com.example.adventure.backend.misc.Messages.*;

@Component
public class Fight {
    public static final String FIGHT = "fight";
    public static final String ATTACK = "attack";
    private final CreatureFilter filter = new CreatureFilter();
    private final Player player;
    private final Tile tile;

    public Fight(final Player player, final Tile tile) {
        this.tile = tile;
        this.player = player;
    }

    public String attack(final String type) {
        final var creaturesTypes = this.filter.getAllCreaturesOfType(tile.getCreatures(), type);
        if (creaturesTypes.isEmpty()) {
            player.setInFight(false);
            return formatF(NO_CREATURE_OF_THAT_TYPE_PRESENT, type);
        }
        return combatPhaseAgainst(creaturesTypes);
    }

    private String combatPhaseAgainst(final List<Creature> cs) {
        final var livingCreatures = this.filter.getAllLivingCreatures(tile.getCreatures());
        final var nextTarget = this.filter.getFirstLivingCreature(cs);
        if (nextTarget == null) {
            return formatF(CREATURES_ARE_ALREADY_DEAD);
        }

        var out = new StringBuilder();
        out.append(characterAttacks(nextTarget));
        if (!nextTarget.isAlive()) {
            final String output = formatF(YOU_KILLED_THE, nextTarget.getName());
            out.append("\n").append(output);
        }

        livingCreatures.stream()
                .filter(Objects::nonNull)
                .filter(Creature::isAlive)
                .forEach(c -> out.append("\n").append(creatureAttacks(c)));
        return out.toString();
    }

    private String creatureAttacks(final Creature creature) {
        final var message = new DamageApplier()
                .withAttacker(creature)
                .withDefender(player)
                .apply();
        return formatF(message);
    }

    public String characterAttacks(final Creature creature) {
        final var message = new DamageApplier()
                .withAttacker(player)
                .withDefender(creature)
                .apply();
        return formatF(message);
    }

    public boolean atLeastOneCreatureIsAlive() {
        return tile.getCreatures().stream()
                .filter(Objects::nonNull)
                .anyMatch(Creature::isAlive);
    }

    public String getEnemyPositionInfo() {
        var out = "";
        var counter = 0;
        var lastName = "";
        for (Creature creature : tile.getCreatures()) {
            counter = isNextEnemyType(lastName, creature) ? counter + 1 : 1;
            final var alive = creature.isAlive() ? "" : "(dead)";
            out += formatF(SYS_CREATURE_LIST_ELEMENT, creature.getName(), counter, alive);
            lastName = creature.getName();
        }
        return out;
    }

    private boolean isNextEnemyType(final String lastName, final Creature c) {
        return lastName.equalsIgnoreCase(c.getName());
    }
}
