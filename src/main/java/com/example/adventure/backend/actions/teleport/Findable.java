package com.example.adventure.backend.actions.teleport;

import java.util.Arrays;

public enum Findable {
    NORTH_DOOR("north door"),
    EAST_DOOR("east door"),
    SOUTH_DOOR("south door"),
    WEST_DOOR("west door");

    private final String name;

    Findable(final String name) {
        this.name = name;
    }

    public static boolean isFindable(final String input) {
        return Arrays.stream(Findable.values()).anyMatch(f -> f.getName().equals(input));
    }

    public String getName() {
        return name;
    }
}
