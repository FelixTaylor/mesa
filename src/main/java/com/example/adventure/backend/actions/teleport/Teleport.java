package com.example.adventure.backend.actions.teleport;

import com.example.adventure.backend.Adventure;
import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.characters.player.Player;
import com.example.adventure.backend.level.components.Tile;
import com.example.adventure.backend.level.rooms.RoomFilter;
import org.springframework.stereotype.Component;

import static com.example.adventure.backend.misc.Messages.*;
import static java.util.Arrays.stream;
import static java.util.Objects.nonNull;

@Component
public class Teleport {
    public static final String TO = "go to";
    private final RoomFilter roomFilter;
    private final Adventure adventure;

    public Teleport(final Adventure adventure) {
        this.roomFilter = new RoomFilter(adventure.getCurrentRoom());
        this.adventure = adventure;
    }

    public String run(final String in) {
        return doAction(in);
    }

    public String askWhereToGo() {
        final StringBuilder out = new StringBuilder();
        stream(adventure.getCurrentRoom().getTiles())
                .forEach(row -> stream(row)
                        .filter(Tile::hasDoor)
                        .forEach(t -> out.append(t.getDoor().getName()).append(", ")));
        return formatN(ENV_ASK_WHERE_TO, out.subSequence(0, out.length()-2));
    }

    public String doAction(final String input) {
        final boolean isFindable = stream(Findable.values())
                .anyMatch(f -> f.getName().equals(input));

        if (isFindable) {
            final String name = input.trim().toUpperCase().replace(" ", "_");
            return toObject(Findable.valueOf(name));
        }
        return EMPTY;
    }

    private Player getPlayer() {
        return this.adventure.getPlayer();
    }

    private String toObject(final Findable input) {
            final Position position = roomFilter.getPositionOfObject(input);
            if (nonNull(position)) {
                getPlayer().setX(position.getX());
                getPlayer().setY(position.getY());
                return formatN(YOU_MOVED_TO, input.getName());
            } else {
                return formatN(YOU_COULD_NOT_GO_TO, input.getName());
            }
    }
}
