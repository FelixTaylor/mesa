package com.example.adventure.backend;

import com.example.adventure.backend.actions.fight.Fight;
import com.example.adventure.backend.actions.move.Move;
import com.example.adventure.backend.actions.teleport.Teleport;
import com.example.adventure.backend.characters.player.Player;
import com.example.adventure.backend.level.Level;
import com.example.adventure.backend.level.TrainingLevel;
import com.example.adventure.backend.level.components.Tile;
import com.example.adventure.backend.level.rooms.Room;
import com.example.adventure.backend.level.rooms.RoomFilter;
import com.example.adventure.backend.main.Config;
import com.example.adventure.backend.misc.InputFilter;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.example.adventure.backend.actions.fight.Fight.ATTACK;
import static com.example.adventure.backend.actions.move.Move.*;
import static com.example.adventure.backend.actions.teleport.Teleport.TO;
import static com.example.adventure.backend.misc.InputFilter.CMD;
import static com.example.adventure.backend.misc.InputFilter.OBJ;
import static com.example.adventure.backend.misc.Messages.*;
import static java.util.Arrays.stream;
import static java.util.Objects.nonNull;

@Data
@Component
public class Adventure {

    @Getter
    private final Config config;

    @Getter
    @Setter
    private Player player;

    @Getter
    @Setter
    private Level level;

    private Fight fight;

    private Teleport teleport;
    private final InputFilter inputFilter = new InputFilter();

    public Adventure(final Config config) {
        this.level = new TrainingLevel().get();
        this.player = new Player();
        this.teleport = new Teleport(this);
        this.config = config;
        this.config.load();
    }

    public Text run(final String input) {
        Text text = new Text();
        System.out.println("input: '" + input + "'");

        if (player.isInFight()) {
            fight(input, text);
        } else if(input.contains(TO) && input.length() > TO.length()) {
            teleportTo(input, text);
        } else {
            actions(input, text);
        }

        if (nonNull(fight) && !fight.atLeastOneCreatureIsAlive()) {
            player.setInFight(false);
        }

        text.setAction(text.getAction() + " " + openDoorsIfConditionsAreMet());
        text.setPositionText(getPositionText());
        text.setTileDescription(getTileDescription());
        return text;
    }

    private String openDoorsIfConditionsAreMet() {
        final Room room = getCurrentRoom();
        final RoomFilter filter = new RoomFilter(room);
        if (filter.canGoThroughNextDoor()) {
            openAllDoors(room);
            if (!room.completionMessageWasShown()) {
                room.completionMessageWasShown(true);
                return formatN(ALL_CREATURES_DEAD_IN_ROOM);
            }
        }
        return EMPTY;
    }

    private void openAllDoors(final Room room) {
        stream(room.getTiles())
                .forEach(tiles -> stream(tiles)
                        .filter(Tile::hasDoor)
                        .forEach(t -> t.getDoor().setOpen(true)));
    }

    private void actions(String input, Text text) {
        switch (trimToLowerCase(input)) {
            case EAST -> text.setAction(moveRight());
            case WEST -> text.setAction(moveLeft());
            case SOUTH -> text.setAction(moveDown());
            case NORTH -> text.setAction(moveUp());
            case ENTER -> text.setAction(moveThrough());
            case GO -> text.setOptions(getMovableDirections());
            case Fight.FIGHT -> text.setAction(initFight());
            case TO -> text.setAction(askWhereToGo());
//            case SAVE: return save();
//            case LOAD: return load();
            default -> text.setError(formatS(SYS_INPUT_IS_NO_VALID_COMMAND, input));
        }
    }

    private void fight(String input, Text text) {
        if (trimToLowerCase(input).equals("cancel")) {
            player.setInFight(false);
        } else {
            switch (inputFilter.filter(CMD, trimToLowerCase(input))) {
                case ATTACK -> text.setAction(fight.attack(inputFilter.filter(OBJ, input)));
                default -> text.setError(formatS(SYS_INPUT_IS_NO_VALID_COMMAND, input));
            }
        }
    }

    private String initFight() {
        fight = new Fight(getPlayer(), getCurrentTile());
        player.setInFight(true);
        return EMPTY;
    }

    private void teleportTo(final String input, final Text text) {
        final var object = input.replace(TO, "").trim();
//        if (isNull(teleport)) {
//            teleport = new Teleport(this);
//        }
        text.setAction(teleport.run(object));
    }

    private String askWhereToGo() {
//        if (isNull(teleport)) {
//            teleport = new Teleport(this);
//        }
        return teleport.askWhereToGo();
    }

    private String moveThrough() {
        final Move move = new Move(this);
        return move.enter();
    }

    private String getPositionText() {
        final var position = getPlayer().getPosition();
        return "[" + position.getX() + "," + position.getY() + "]";
    }

    private String moveUp() {
        final Move move = new Move(this);
        return move.up();
    }

    private String moveDown() {
        final Move move = new Move(this);
        return move.down();
    }

    private String moveLeft() {
        final Move move = new Move(this);
        return move.left();
    }

    private String moveRight() {
        final Move move = new Move(this);
        return move.right();
    }

    private String getMovableDirections() {
        final Move move = new Move(this);
        final String directions = move.getPossibleDirectionsToMove(getPlayer());
        return formatN(WHERE_TO_GO) + " " + formatN(directions);
    }

    public Room getCurrentRoom() {
        return getLevel().getRoom(player.getCurrentRoomId());
    }

    public Tile getCurrentTile() {
        final int x = getPlayer().getX();
        final int y = getPlayer().getY();
        return getCurrentRoom().getTiles()[x][y];
    }

    public String getTileDescription() {
        String out = "";
        if (config.isDeveloper()) {
            out = getCurrentTile().toString();
        }
        out += getCurrentTile().getDescription();
        return out;
    }
}
