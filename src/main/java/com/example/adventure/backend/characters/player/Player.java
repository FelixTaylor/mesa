package com.example.adventure.backend.characters.player;

import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.characters.LivingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import static com.example.adventure.backend.characters.creatures.Attack.*;

@ToString
@Component
public class Player extends LivingEntity {

    @Getter
    private final Position position;

    @Getter
    @Setter
    private int currentRoomId;

    @Getter
    @Setter
    private boolean inFight;

    public Player() {
        super();
        this.position = new Position(0,0);
        this.currentRoomId = 1;
        this.inFight = false;
        setAttacks(HIT, STAB, SLASH);
        setHitPoints(100);
        setName("Player");
    }

    public int getX() {
        return getPosition().getX();
    }

    public int getY() {
        return getPosition().getY();
    }

    public void setX(final int x) {
        getPosition().setX(x);
    }

    public void setY(final int y) {
        getPosition().setY(y);
    }

}
