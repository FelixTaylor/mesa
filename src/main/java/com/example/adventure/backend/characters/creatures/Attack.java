package com.example.adventure.backend.characters.creatures;

import lombok.Getter;

@Getter
public enum Attack {
    NONE("ATTACK NOT SET", 0),
    SCRATCH("scratches", 10),
    JUMP("jumps", 5),
    BITE("bites", 30),


    STAB("stab", 15),
    HIT("hit", 5),
    SLASH("slash", 10);

    private final String name;
    private final double baseDamage;

    Attack(final String name, final int baseDamage) {
        this.name = name;
        this.baseDamage = baseDamage;
    }
}
