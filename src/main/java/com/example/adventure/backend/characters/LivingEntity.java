package com.example.adventure.backend.characters;

import com.example.adventure.backend.actions.fight.AttackDamageContainer;
import com.example.adventure.backend.characters.creatures.Attack;
import com.example.adventure.backend.characters.creatures.AttackModifier;
import com.example.adventure.backend.misc.GameObject;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static com.example.adventure.backend.characters.creatures.AttackModifier.MEDIUM;
import static java.lang.Math.max;
import static java.util.Arrays.*;

public class LivingEntity extends GameObject {

    @Getter
    private ArrayList<Attack> attacks;

    @Setter
    private AttackModifier modifier;

    @Getter
    private double hitPoints;

    public LivingEntity() {
        super();
        this.hitPoints = 0;
        this.modifier = MEDIUM;
    }

    public void setHitPoints(final double hitPoints) {
        this.hitPoints = max(hitPoints, 0.0);
    }

    public void reduceHealth(final double health) {
        final double newHealth = this.hitPoints - health;
        this.hitPoints = max(newHealth, 0.0);
    }

    public void setAttacks(Attack... attacks) {
        this.attacks = new ArrayList<>();
        this.attacks.addAll(asList(attacks));
    }

    public AttackDamageContainer getAttackDamage() {
        final int random = new Random().nextInt(getAttacks().size());
        final Attack attack = getAttacks().get(random);
        final double damage = attack.getBaseDamage() * getModifier();
        return new AttackDamageContainer(attack, damage);
    }

    public boolean isAlive() {
        return this.hitPoints > 0;
    }

    public double getModifier() {
        return modifier.getModifier();
    }
}
