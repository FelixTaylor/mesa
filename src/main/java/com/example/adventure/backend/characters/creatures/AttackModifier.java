package com.example.adventure.backend.characters.creatures;

import lombok.Getter;

@Getter
public enum AttackModifier {
    TINY(.25), SMALL(.5), MEDIUM(1.0), LARGE(1.5), GIGANTIC(2.0);

    private final double modifier;

    AttackModifier(final double modifier) {
        this.modifier = modifier;
    }

}
