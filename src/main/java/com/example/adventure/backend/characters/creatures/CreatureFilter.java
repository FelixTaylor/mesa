package com.example.adventure.backend.characters.creatures;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;

public class CreatureFilter {
    public CreatureFilter() { }

    public List<Creature> getAllCreaturesOfType(final ArrayList<Creature> creatures, final String type) {
        if (type == null || type.trim().isEmpty())
            throw new IllegalArgumentException("The given type value may NOT be null or empty.");

        return creatures.stream()
                .filter(equalType(type))
                .collect(toList());
    }

    private Predicate<Creature> equalType(final String type) {
        return c -> c.getName().equalsIgnoreCase(type.trim());
    }

    public List<Creature> getAllLivingCreatures(final List<Creature> creatures) {
        return of(creatures.toArray(Creature[]::new))
                .filter(Creature::isAlive)
                .collect(toList());
    }

    public Creature getFirstLivingCreature(final List<Creature> creatures) {
        return of(creatures.toArray(Creature[]::new))
                .filter(Creature::isAlive)
                .findFirst()
                .orElse(null);
    }
}
