package com.example.adventure.backend.characters;

import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.characters.creatures.Attack;
import com.example.adventure.backend.characters.creatures.AttackModifier;
import com.example.adventure.backend.characters.player.Player;

public class CharacterBuilder {
    private final Player player;
    private double baseHealth = 100;

    public CharacterBuilder() {
        this.player = new Player();
    }

    public CharacterBuilder withName(final String name) {
        player.setName(name);
        return this;
    }

    public CharacterBuilder withAttacks(final Attack... attacks) {
        player.setAttacks(attacks);
        return this;
    }

    public CharacterBuilder withAttackModifier(final AttackModifier modifier) {
        player.setModifier(modifier);
        return this;
    }

    public CharacterBuilder withRoomId(final int roomId) {
        player.setCurrentRoomId(roomId);
        return this;
    }

    public CharacterBuilder withPosition(final Position position) {
        player.setX(position.getX());
        player.setY(position.getY());
        return this;
    }

    public CharacterBuilder withDescription(final String description) {
        player.setDescription(description);
        return this;
    }

    public CharacterBuilder withBaseHealth(final double baseHealth) {
        this.baseHealth = baseHealth;
        return this;
    }

    public Player build() {
        final double modifier = player.getModifier();
        final double health = baseHealth * modifier;
        player.setHitPoints(health);
        return player;
    }
}
