package com.example.adventure.backend.characters.creatures;


import org.springframework.context.annotation.Bean;

import static com.example.adventure.backend.characters.creatures.Attack.*;
import static com.example.adventure.backend.characters.creatures.AttackModifier.SMALL;
import static com.example.adventure.backend.characters.creatures.AttackModifier.TINY;

public class CreatureFactory {

    @Bean
    public static Creature getRat() {
        return new CreatureBuilder()
                .withName("Rat")
                .withDescription("A small rat with brown grey fur.")
                .withAttacks(SCRATCH, BITE)
                .withModifier(TINY)
                .withBaseHealth(35)
                .build();
    }

    @Bean
    public static Creature getMeatBug() {
        return new CreatureBuilder()
                .withName("Meat Bug")
                .withDescription("A creepy little red bug.")
                .withAttacks(JUMP, BITE)
                .withModifier(TINY)
                .withBaseHealth(25)
                .build();
    }

    @Bean
    public static Creature getFeralDog() {
        return new CreatureBuilder()
                .withName("Feral Dog")
                .withDescription("A hungry looking dog with a little blood around his mouth.")
                .withAttacks(BITE, SCRATCH)
                .withModifier(SMALL)
                .withBaseHealth(50)
                .build();
    }
}
