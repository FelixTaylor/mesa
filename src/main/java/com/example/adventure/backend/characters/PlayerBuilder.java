package com.example.adventure.backend.characters;

import com.example.adventure.backend.actions.move.Position;
import com.example.adventure.backend.characters.player.Player;

import static com.example.adventure.backend.characters.creatures.Attack.*;
import static com.example.adventure.backend.characters.creatures.AttackModifier.MEDIUM;

public class PlayerBuilder {
    private static String name = "Player";
    public static Player get() {
        return new CharacterBuilder()
                .withName(name)
                .withDescription("This is you")
                .withPosition(new Position(0,0))
                .withAttacks(SLASH, HIT, STAB)
                .withAttackModifier(MEDIUM)
                .withRoomId(1)
                .build();
    }

    public static String name() {
        return name;
    }
}
