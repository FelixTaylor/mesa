package com.example.adventure.backend.characters.creatures;

public class CreatureBuilder {
    private final Creature creature;
    private double baseHealth = 100;

    public CreatureBuilder() {
        this.creature = new Creature();
    }

    public CreatureBuilder withName(final String name) {
        creature.setName(name);
        return this;
    }

    public CreatureBuilder withAttacks(final Attack... attacks) {
        creature.setAttacks(attacks);
        return this;
    }

    public CreatureBuilder withDescription(final String description) {
        creature.setDescription(description);
        return this;
    }

    public CreatureBuilder withModifier(final AttackModifier modifier) {
        creature.setModifier(modifier);
        return this;
    }

    public CreatureBuilder withHitPoints(double hitPoints) {
        creature.setHitPoints(hitPoints);
        return this;
    }

    public CreatureBuilder withBaseHealth(final double baseHealth) {
        this.baseHealth = baseHealth;
        return this;
    }

    public Creature build() {
        if (creature.getHitPoints() == 0) {
            final double modifier = creature.getModifier();
            final double health = baseHealth * modifier;
            creature.setHitPoints(health);
            return creature;
        }
        return creature;
    }
}
