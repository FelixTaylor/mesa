package com.example.adventure.backend.characters.player;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Statistics {
    private int creaturesKilled;
    private int moves;

    public Statistics() {
        this.creaturesKilled = 0;
        this.moves = 0;
    }

    public void addCreatureKill() {
        this.creaturesKilled++;
    }

    public void addMove() {
        this.moves++;
    }
}
