package com.example.adventure.controller;

public class FrontEndVariables {
    public static final String SYS_OUTPUT_KEY = "output";
    public static final String SYS_DESCRIPTION_KEY = "description";
    public static final String SYS_PLAYER_POS_KEY = "player_position";
    public static final String KEY_ADVENTURE = "adventure" ;
    public static final String KEY_HITPOINTS = "hitpoints" ;
    public static final String KEY_TEXT = "text" ;
}
