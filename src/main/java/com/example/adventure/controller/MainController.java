package com.example.adventure.controller;


import com.example.adventure.backend.Adventure;
import com.example.adventure.backend.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static com.example.adventure.controller.FrontEndVariables.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class MainController {
    private static final String NAME = "mesa";
    public static final String PATH = "/" + NAME;

    @Autowired
    private Adventure adventure;

    @RequestMapping(value = PATH, method = GET)
    public String home(final Model model) {
        final var text = new Text();
        model.addAttribute(KEY_TEXT, text);
        model.addAttribute(KEY_ADVENTURE, this.adventure);
        text.setTileDescription(adventure.getLevel().getDescription());
        model.addAttribute(KEY_HITPOINTS, this.adventure.getPlayer().getHitPoints());
        return NAME;
    }

    @RequestMapping(value= PATH, method = POST)
    public String home(final Model model, @RequestParam("user_input") final String input) {
        final var text = adventure.run(input);
        model.addAttribute(KEY_TEXT, text);
        model.addAttribute(KEY_ADVENTURE, adventure);
        model.addAttribute("creatures", adventure.getCurrentTile().getCreatures());
        model.addAttribute(KEY_HITPOINTS, adventure.getPlayer().getHitPoints());
        return NAME;
    }
}
