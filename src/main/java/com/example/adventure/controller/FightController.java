package com.example.adventure.controller;

import com.example.adventure.backend.actions.fight.Fight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class FightController {
    public static final String NAME = "fight";
    public static final String PATH = "/" + NAME;

    private static final String SYS_CREATURE_LIST = "creature_list";

    @Autowired
    private Fight fight;

    @RequestMapping(value = PATH, method = GET)
    public String initFight(final Model model) {
        System.out.println("initFight GET");
        model.addAttribute(SYS_CREATURE_LIST, fight.getEnemyPositionInfo());
//        model.addAttribute(SYS_PLAYER_POS_KEY, posText);
        model.addAttribute(NAME, fight);
        return NAME;
    }

    @RequestMapping(value=PATH, method= POST)
    public String fight(final Model model, @RequestParam("user_input") final String input) {
        System.out.println("initFight POST  ");
        model.addAttribute(SYS_CREATURE_LIST, fight.getEnemyPositionInfo());
//        model.addAttribute(SYS_PLAYER_POS_KEY, posText);
//        model.addAttribute(SYS_OUTPUT_KEY, fight.run(input));
        return NAME;
    }

}
