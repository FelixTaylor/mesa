body {
  position: absolute;
  font-size: 16px;
  left: 0;
  top: 0;
}
body .app {
  border: 1px solid red;
  position: relative;
  margin: 2rem auto 0 auto;
  width: 20rem;
  left: 0;
  top: 0;
}
body .app #user_input {
  width: 100%;
}

#player_position {
  font-family: SansSerif, sans-serif;
  position: relative;
  font-size: 0.5rem;
  left: 0;
  top: 0;
}

/*# sourceMappingURL=main.cs.map */
