# Changelog

## 0.5.0

### 2020-XX-xx



---

## 0.4.0

### 2020-12-15

- Game is now a Spring Boot project
- Implemented Sass
- Implemented Theymeleaf
- Implemented TrainingLevel class
- Implemented TileBuilder, RoomBuilder, CreatureBuilder, CharacterBuilder and LevelBuilder
- Implemented Describable, Fighting and Moving interfaces
- Implemented Test classes
- Implemented createSaveFolderIfNotPresent method in main class
- Implemented Findables
- Implemented 'go to' action
- Implemented first iteration of Docker
- Implemented bidirectional doors
- Added first iteration of UI
- Updated README.md
- Updated output text
- Removed primary and secondary attack. Creatures now have just multiple attacks
