FROM adoptopenjdk/openjdk15
ARG JAR_FILE=MESA/out/artifacts/MESA_jar/MESA.jar
WORKDIR /usr/local/runme
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","--enable-preview","app.jar"]
